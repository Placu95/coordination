package it.unibo.coordination.linda.core;

public enum OperationPhase {
    INVOCATION,
    COMPLETION;
}
